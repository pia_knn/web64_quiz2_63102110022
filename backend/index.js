const express = require('express')
const mysql = require('mysql')
const bcrypt = require('bcrypt')

// connect to database
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'music_school',
    port: '3306'
})

connection.connect(err => {
    if (err) {
        console.log(err)
        return
    }
    console.log('mysql connected!')
})

// initial 
const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// applicants
app.post('/register', async (req, res) => {
    const { name, age, number, email, password, confirmPassword, interest } = req.body
    try {
        if (password != confirmPassword) {
            return res.status(400).json({ message: "password must match" })
        }
        const hashedPassword = await bcrypt.hash(password, 10)
        connection.query(
            "SELECT * FROM member WHERE m_name = ?",
            [name],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                if (result.length) {
                    return res.status(400).json({ message: "User already exist!" }).send()
                } else {
                    connection.query(
                        "INSERT INTO applicants (a_name, a_age, a_number, a_email, a_password, a_interest) VALUES (?, ?, ?, ?, ?, ?)",
                        [name, age, number, email, hashedPassword, interest],
                        (error, result, field) => {
                            if (error) {
                                console.log(error)
                                return res.status(400).send()
                            }
                            return res.status(201).json({ message: "Apply for music school successfully!" })
                        }
                    )
                }
            }
        )
    } catch (error) { 
        console.log(error)
        return res.status(500).send()
    }
})

app.post('/applicant/approve/:id', async (req, res) => {
    const { id } = req.params
    try {
        connection.query(
            "SELECT * FROM applicants WHERE a_id = ?",
            [id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                result = Object.assign({}, result[0]);
                if (!result) {
                    return res.status(400).json({ message: "Cannot find user!" })
                } else {
                    connection.query(
                        "INSERT INTO member (m_name, m_age, m_number, m_email, m_password, m_interest, m_status) VALUES (?,?,?,?,?,?,?)",
                        [result.a_name, result.a_age, result.a_number, result.a_email, result.a_password, result.a_interest, "user"],
                        (error, result1, field) => {
                            if (error) {
                                console.log(error)
                                return res.status(400).send()
                            } else {
                                connection.query(
                                    "DELETE FROM applicants WHERE a_id = ?",
                                    [id],
                                    (error, result, field) => {
                                        if (error) {
                                            console.log(error)
                                            return res.status(400).send()
                                        }
                                    }
                                )
                            }
                            return res.status(200).json({ message: "Approve successfully!" })
                        }
                    )
                }
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.get('/applicants', async (req, res) => {
    try {
        connection.query(
            "SELECT * FROM applicants",
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.get('/applicant/:id', async (req, res) => {
    const { id } = req.params
    try {
        connection.query(
            "SELECT * FROM applicants WHERE a_id = ?",
            [id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.get('/applicant/interest/:interest', async (req, res) => {
    const { interest } = req.params
    try {
        connection.query(
            "SELECT * FROM applicants WHERE a_interest = ?",
            [interest],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.delete('/applicant/:id', async (req, res) => {
    const { id } = req.params
    try {
        connection.query(
            "DELETE FROM applicants WHERE a_id = ?",
            [id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                if (result.affectedRows === 0) {
                    res.status(400).json({ message: "No user founded!" })
                }
                return res.status(200).send()
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

// member
app.get('/member', async (req, res) => {
    try {
        connection.query(
            "SELECT * FROM member",
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.get('/member/interest/:interest', async (req, res) => {
    const { interest } = req.params
    try {
        connection.query(
            "SELECT * FROM member WHERE m_interest = ?",
            [interest],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.get('/member/:id', async (req, res) => {
    const { id } = req.params
    try {
        connection.query(
            "SELECT * FROM member WHERE m_id= ?",
            [id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                return res.status(200).json(result)
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.delete('/member/:id', async (req, res) => {
    const { id } = req.params
    try {
        connection.query(
            "DELETE FROM member WHERE m_id = ?",
            [id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).send()
                }
                if (result.affectedRows === 0) {
                    res.status(400).json({ message: "No user founded!" })
                }
                return res.status(200).send()
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

app.put('/member/:id', async (req, res) => {
    const { id } = req.params
    const { name, age, number, email, interest } = req.body
    try {
        connection.query(
            "UPDATE member SET m_name = ?, m_age = ?,m_number = ?, m_email = ?, m_interest = ? WHERE m_id = ?",
            [name, age, number, email, interest, id],
            (error, result, field) => {
                if (error) {
                    console.log(error)
                    return res.status(400).json({ message: "Fail to update!" })
                }
                return res.status(200).json({ message: "Update successfully!" })
            }
        )
    } catch (error) {
        console.log(error)
        return res.status(500).send()
    }
})

const port = process.env.PORT || 3001
app.listen(port, () => console.log('server running on port 3001'))